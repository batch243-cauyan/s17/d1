


function printName(){
    console.log("My name is John.")
    
}


// Function Invocation
printName();


// Function declaration. 
    // Function declaration
        // A function can be created through function declaration by
        // using the keyword function and adding function name.

        // Declared functions are not executed immediately.

        declaredFunction();

        function declaredFunction(){
            console.log("Hello World from declaredFunction");
        }

// function expression
        // functions that are stored in a variable is called a function expression.


//       Anonymous function - functions without a name.

let variableFunction = function(){
    console.log("Hello for variableFunction!")
}


variableFunction();

let funcExpression = function funcName(){
    console.log(5+2);
    console.log("Hello from funcExpression.");
}

funcExpression();

// You can reassign declated functions and function expression to new anonymous function.

declaredFunction = function (){
    console.log("Updated declaredFunction");
}

declaredFunction();

funcExpression = function(){
    console.log("Updated hello from funcExpression.");
}

funcExpression();


// Function expression using const keyword
const constantFunc = function(){
    console.log("Initialized with const!")
}

constantFunc();


// Function Scoping 

// Scope is accessibility of variable within our program.
// Javascript variables, has 3 types: local/block scope
// 2. global scope. 3. Function scope.


{let localVar = "Armando Perez"}
let globalVar = "Mr.Worldwide";
// console.log(localVar);


let multiply = function(num1,num2){
    return num1*num2;
}

console.log(multiply(5,3))

// Function scope
// Javascript has function scope: Each function creates a new scope.
// Variables defined inside a function are not accessible outside the function.

// function showNames(){
//     let functionLet ="Jane";
//     const functionConst = "John";

//     console.log(functionConst);
//     console.log(functionLet);
//     return functionLet;
//     }

// showNames();

// The variables, functionLet, and functionConst are not accessible outside the function that they were declared in.

// Nested Functions
    // You can create another function inside a function

function myNewFunction(){
    let name = "Jane";
    nestedFunction();
    function nestedFunction(){
        let nestedName = "John";

        console.log(nestedName);
        console.log(name);
    }
}

myNewFunction();

//  Function and Global Scope Variables
    // Global Scoped Variable
    let globalName = "Alexandro";
    
    function myNewFunction2(){
        let nameInside = "Renz";
        console.log(globalName);
        console.log(nameInside);

    }

myNewFunction2();


// using alert.
    // alert() allows us to show a small window at the top of
    // our browser page to show information to our users. As opposed to console.log()

// alert("Hello World"); //This will run immediately when page loads.

// function showSampleAlert(){
//     alert("Hello User");
// }

// showSampleAlert();

//  let fName = prompt("What is your name?");
//  alert("Welcome "+ fName);

//  Notes on use of alert()
    // Show only an alert() for short dialogs/messages to the user.
    // Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// Prompt()
    let samplePrompt = prompt("Enter your name: ");
    // console.log(samplePrompt);


    // console.log (typeof(samplePrompt));


    function printWelcomeMessages(){
        let firstName = prompt("Enter your First Name: ");
        let lastName = prompt("Enter your Last Name: ");

        console.log(`Hello ${firstName} ${lastName}, Welcome to my page.`);
    }

    printWelcomeMessages();

    // Section, Function Naming Convention
        // Function names should be definitive of the task it will perform. It is
        // usually contains a verb.

        function getCourses(){
            let course = ["Science 101", "Math 101", "English 101"];

            console.log(course);
        }

        getCourses();
    
        // Avoid generic names to avoid confusion within your code/program

        function displayCarInfo(){
            console.log("Brand: Toyota");
            console.log("Type: Sedan");
            console.log("Price: 1,500,000.00");
        }

        displayCarInfo();
        

